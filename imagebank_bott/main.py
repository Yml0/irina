import asyncio
from aiogram import Bot, Dispatcher
from aiogram.types.message import ContentType
from aiogram.contrib.middlewares.logging import LoggingMiddleware

from chat import ChatManager
import config as cfg

class Main:

	def __init__(self):
		try:
			asyncio.run(self.main())
		except KeyboardInterrupt:
			self.db.save()
			exit(0)

	async def main(self):
		self.bot = Bot(token=cfg.botToken)
		dp = Dispatcher(self.bot)
		dp.middleware.setup(LoggingMiddleware())

		self.chatManager = ChatManager(self)

		await self.register_handlers(dp)

		print("Бот запущен.")
		await dp.start_polling()

	async def register_handlers(self, dp):
		dp.register_message_handler(self.chatManager.onStartCommand, commands=['start'], content_types=ContentType.TEXT)
		dp.register_message_handler(self.chatManager.onPhoto, content_types=ContentType.PHOTO)
		dp.register_errors_handler(self.chatManager.onException, exception=None)


if __name__ == '__main__':
	Main()