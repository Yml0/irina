import os
import hashlib
import random
from uuid import uuid4
from aiogram import types

class ChatManager:

	temp_path = "temp"
	storage_path = "storage"

	def __init__(self, inst):
		self.inst = inst

	async def onStartCommand(self, message: types.Message):
		await message.reply("Пришлите боту картинку, и произойдет нечто потрясающее.", reply=False)
	
	def sha1sum(self, path):
		h = hashlib.sha1()
		with open(path, 'rb') as f:
			h.update(f.read())
		return h.hexdigest()

	async def onPhoto(self, message: types.Message):
		file_id = message.photo[-1]["file_id"]
		temp_path = f"{self.temp_path}/{uuid4()}.jpg"
		await self.inst.bot.download_file_by_id(file_id, temp_path)

		h = self.sha1sum(temp_path)
		new_name = f"{h}.jpg"
		files = os.listdir(self.storage_path)
		if new_name not in files:
			os.rename(temp_path, f"{self.storage_path}/{new_name}")
			files.append(new_name)

		randomImageName = random.choice(files)
		randomImage = open(f"{self.storage_path}/{randomImageName}", 'rb')
		await message.reply_photo(photo=randomImage, reply=False)

	async def onException(self, event, exception):
		print(exception)