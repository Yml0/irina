import asyncio
from aiogram import Bot, Dispatcher, types
from aiogram.types.message import ContentType
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.types import ParseMode

import perem
import config as cfg

import logging
logging.basicConfig(level=logging.INFO)

alphabet = 'qph4ая5ЇPxNждAЗ(Cт_ЬъФ6Hї#Ъ9SVАгТ$Рй),kоt|Єи03ЯбпУ+IG>2вґл:єJЕvЭЖхlҐЩoПГcFgчwQbенKi[TЫY7eЮркм8^зфы;ЙБOM%.ЧrXB~]ё\\m сzn\'ОИ*щaуЦZ-dULWDц@ШВшэС!/us&НE}f=RЁ1{<Дюj"ХКМ?ьyЛ'
alphabet_len = len(alphabet)


def clear_screen():
	for _ in range(200):
		print()

def name():
	clear_screen()
	print()
	for s in perem.name:
		print(s)
	print()

def caesar(text, key):
	text_ = ''
	for i in range(len(text)):
		text_ += alphabet[(alphabet.index((text[i])) + key) % alphabet_len]
	return text_

bot = Bot(token=cfg.botToken)
dp = Dispatcher(bot)

keys = {}

markup = InlineKeyboardMarkup()
markup.row(
	InlineKeyboardButton("Зашифровать", callback_data='encrypt'),
	InlineKeyboardButton("Расшифровать", callback_data='decrypt')
)

@dp.message_handler(commands=["start"])
async def onStartCommand(message: types.Message):
	await message.answer("Пришлите текст, с которым бот будет работать.\n`/key <значение>` — установить ключ.", parse_mode=ParseMode.MARKDOWN)

@dp.message_handler(commands=["key"])
async def onKeyCommand(message: types.Message):
	args = message.text.split()

	errorMsg = "Синтаксис: `/key <значение>`\nПример: `/key 6`"
	if len(args) != 2:
		return await message.answer(errorMsg, parse_mode=ParseMode.MARKDOWN)
	try:
		key = int(args[1])
	except Exception:
		return await message.answer(errorMsg, parse_mode=ParseMode.MARKDOWN)

	keys[message['from']['id']] = key
	return await message.reply(f"Ключ установлен.")

@dp.message_handler(content_types=ContentType.TEXT)
async def onText(message):
	await message.delete()
	await message.answer(f"`{message.text}`", reply_markup=markup, parse_mode=ParseMode.MARKDOWN)

@dp.callback_query_handler(lambda c: c.data == 'encrypt')
async def onEncryptCallback(query: types.CallbackQuery):
	message = query.message
	user_id = query['from']['id']
	try:
		text = caesar(message.text, keys.get(user_id, cfg.defaultKey))
		await message.edit_text(f"`{text}`", reply_markup=markup, parse_mode=ParseMode.MARKDOWN)
		await query.answer()
	except Exception as e:
		await query.answer("Текст не поддаётся зашифровке!")

@dp.callback_query_handler(lambda c: c.data == 'decrypt')
async def onDecryptCallback(query: types.CallbackQuery):
	message = query.message
	user_id = query['from']['id']
	try:
		text = caesar(message.text, -keys.get(user_id, cfg.defaultKey))
		await message.edit_text(f"`{text}`", reply_markup=markup, parse_mode=ParseMode.MARKDOWN)
		await query.answer()
	except Exception as e:
		await query.answer("Текст не поддаётся расшифровке!")

name()
executor.start_polling(dp, skip_updates=True)