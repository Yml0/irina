from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
import random

bot = Bot(token='5327611724:AAEuOx0BgdsSolZq0enrKknLc1bDeJE9YEE')
dp = Dispatcher(bot)

users = {}

menu = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text='/start')
        ]   
    ],
    resize_keyboard=True
)
menu1 = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text='1'),
            KeyboardButton(text='2'),
            KeyboardButton(text='3'),
            KeyboardButton(text='4'),
            KeyboardButton(text='5')
        ],      
    ],
    resize_keyboard=True
)

@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    users[message['from']['id']] = str(random.randint(1, 5))
    await message.reply("Я загадал число от 1 до 5, отгадай его", reply_markup=menu1)

@dp.message_handler(content_types=['text'])
async def shh(message:types.Message):
    if message['from']['id'] in users:
        if users[message['from']['id']] == message.text:
            del users[message['from']['id']]
            await message.reply('Правильно. Нажми на кнопку старт, чтобы сыграть еще раз.', reply_markup=menu)
        else:
            await message.reply('Неправильно, попробуй еще раз', reply_markup=menu1)

executor.start_polling(dp, skip_updates=False)